import axios from "axios";

const ENDPOINT = "https://viacep.com.br/ws/";

export function getAddress(cep) {
  
  return function(dispatch){

    axios.get(ENDPOINT+cep+"/json")
      .then((response)=>{

        if(response.data.erro===true){

          dispatch({
            type: "SHOW_ERROR",
            payload: false
          });

        }else{

          dispatch({
            type: "FETCH_ADDRESS",
            payload: response.data
          });

        }
        
      })
      .catch((err)=> {
          console.log('erro: ', err)
      })

  };
}
