function address(state = {
  address: [],
}, action){

  switch(action.type) {

    case "SHOW_ERROR" :
      state = { address: false };

	case "FETCH_ADDRESS" :
	  state = { address: action.payload };

    default:
      return state;
  }
}

export default address;
