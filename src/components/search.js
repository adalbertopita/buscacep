import React, { Component } from 'react';
import {connect} from "react-redux";
import { getAddress } from "../actions/addressActions";

import MaskedInput from "react-maskedinput";

class Search extends Component {

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e){
    e.preventDefault();
    let el = document.querySelector(".search-text"),
        cep = el.value;

    this.props.getAddress(cep);

  }

  render() {
    return (
      <div className="search-form">
      <h2 className="title-small">Consultar</h2>
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="search-text">CEP</label>
          <MaskedInput mask="11111111" type="text" placeholder="digite um CEP" className="search-text" />
          <button type="submit" className="btn">buscar</button>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAddress: (cep) => {
            dispatch(getAddress(cep));
        }
    };
};

export default connect(null, mapDispatchToProps)(Search);
