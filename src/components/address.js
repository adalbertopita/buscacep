import React, { Component } from 'react';

import ErrorMessage from './errorMessage';

class Address extends Component {

  constructor(props) {
    super(props);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose(){
    let el = document.querySelector(".address-wrapper");
    el.style.display = "none";
  }

  render() {

    const address = this.props.data,
      gmapsKey = "AIzaSyBBOw2AobJyV2OMftG9f4aCRxzL6CsMA8o",
      mapsEmbed = `https://www.google.com/maps/embed/v1/place?key=${gmapsKey}&q=${address.logradouro},${address.bairro},${address.localidade}+${address.uf}`;
    
    if(address.length===0 || address===false){
      
      let showError;
      
      if(address===false){
        showError = true;
      }

      return (
        <ErrorMessage showError={showError}/>
      );
    }else{
      return (
          <div className="address-wrapper">
            <a onClick={this.handleClose} className="btn-close"><i className="fa fa-times" aria-hidden="true"></i></a>
            <h3 className="title-address">{address.logradouro}</h3>
            <p className="address-bairro">{address.bairro}</p>
            <p>{address.localidade} - {address.uf}</p>
            <p>{address.cep}</p>

            <iframe
              width="400"
              height="250"
              src={mapsEmbed}>
            </iframe>

          </div>
        );
    }
  }
}

export default Address
