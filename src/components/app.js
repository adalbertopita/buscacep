import React, { Component } from 'react';
import {connect} from "react-redux";

//Components
import Search from "./search";
import Address from "./address";

class App extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="title">Consulta de Endereço</h1>
        <Search />
        <Address data={this.props.address} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      address: state.address
  };
};

export default connect(mapStateToProps)(App);
