import {createStore, combineReducers, applyMiddleware} from "redux";
//import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import addressReducer from "./reducers/addressReducer";

const store = createStore(addressReducer,
      applyMiddleware(
        promise(),
        //logger(),
        thunk
      )
);

export default store;
