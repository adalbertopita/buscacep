import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";

//Styles
const css = require('./sass/main.scss');

//Store
import store from './store';

//Main Component
import App from "./components/app";

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
