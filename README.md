## App para consulta de CEP via API ##

Desenvolvido com React/Redux.

Para instalar a aplicação é necessário ter instalado NodeJS e Npm [(instruções aqui)](https://docs.npmjs.com/getting-started/installing-node)

Após instalados execute *npm install*

Para executar utilize **npm start** e acesse o link **http://localhost:8080**

A versão dos arquivos minimizados para prod, basta executar o comendo **npm run prod**